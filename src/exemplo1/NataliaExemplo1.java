/**
 * Exemplo1: Programacao com threads
 * Autor: Lucio Agostinho Rocha
 * Ultima modificacao: 07/08/2017
 */
package exemplo1;

import java.util.HashMap;
import java.util.Random;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

/**
 *
 * @author Natália
 */
public class NataliaExemplo1  {

        public static void main(String [] args){
            
            System.out.println("Inicio da criacao das threads!");
            
            ExecutorService obj = Executors.newCachedThreadPool();
            
            HashMap v = new HashMap();
            
            //Cria cada thread com um novo runnable selecionado
            Thread t;
            for (int i=0; i<20; i++){
                t= new Thread(new NataliaPrintTasks(i, v));
                //Inicia as thread e as coloca no estado executavel
                obj.execute(t);
            }
      
             
            
            System.out.println("Threads criadas");
            
            obj.shutdown();
            
            try{
            obj.awaitTermination(1, TimeUnit.DAYS);
            }catch(InterruptedException e){
            e.printStackTrace();
            }
            
            float menorTempo = (float) v.get(0);
            
            int vencedor = 0;
                    
            for (int i=0; i<20; i++){
            float r = (float) v.get(i);
            if(menorTempo > r){
            menorTempo = r;
            vencedor = i;
       
            }
            }
        
     
                System.out.printf("Carro vencedor : %d, tempo : %f", vencedor, menorTempo);

        }
}
        
