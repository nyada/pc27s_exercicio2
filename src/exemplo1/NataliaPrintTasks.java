/**
 * Exemplo1: Programacao com threads
 * Autor: Lucio Agostinho Rocha
 * Ultima modificacao: 07/08/2017
 */
package exemplo1;

import java.util.HashMap;
import java.util.Random;
import static javax.swing.UIManager.put;

/**
 *
 * @author Lucio
 */
public class NataliaPrintTasks implements Runnable {

    
    private final HashMap v;
    private final static Random generator = new Random();
    private  int ID=0;
    float tempo = 0;
    
    public NataliaPrintTasks(int ID, HashMap v){
        
        this.ID = ID;
        this.v = v;
    }
   
    
    @Override
    public void run(){
        try{
          
            for (int i = 0; i< 5 ; i++){
                int sleepTime = generator.nextInt(1000); 
                Thread.sleep(sleepTime);
                this.tempo += sleepTime;
                System.out.printf("voltas[%d]:%d ms tempo total: %2f ms\n",(i +1), sleepTime, this.tempo);
                 this.v.put(this.ID, this.tempo);
              
            }
            
            
            
        } catch (InterruptedException ex){
            System.out.printf("%s %s\n",ID , "terminou de maneira inesperada.");
        }
       System.out.printf("%s Uhuuuuuuul acabou!!\n", ID);
    }
       
}
